/*******************************************************************************
 * Copyright (c) 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package openin.sampleopen;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPart;

public class OpenIn implements IObjectActionDelegate {
	
	private ISelection selection;
	private Shell shell;
	private static final String CMD = "cmd.exe /c start cmd.exe /k \"";
	private static final String EXPLORER_WINXP = "explorer ";
	private static final String GNOME = "gnome-terminal --working-directory=";
	private static final String EXPLORER_LINUX_UBUNTU = "nautilus ";
	
	// Operating System
	String osName = System.getProperty("os.name");
	String WINDOWS = "Windows";  //Windows Xp
	String LINUX = "Linux";

	public OpenIn() {
		super();
	}

	@Override
	public void setActivePart(IAction arg0, IWorkbenchPart arg1) {
		// TODO Auto-generated method stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run(IAction action) {
		String text = action.getText();
		//tooltip is menu text -- Open In
		// selectionid is plugin.xml action id
//		String tooltip = action.getToolTipText();
//		String selectionid = action.getId();
		
		if (selection instanceof IStructuredSelection) {
			for (Iterator<Object> it = ((IStructuredSelection) selection).iterator(); it.hasNext();) {
				Object element = it.next();
				String path = getSelectionPath(element);
				if (path != null) {
					try {
						if("Explorer".equalsIgnoreCase(text)){
							if(osName.contains(WINDOWS)){
								openExplorerWinXp(path);
							}else if(osName.contains(LINUX)){
								openExplorerLinux(path);
							} else{
								MessageDialog.openInformation(
					                    shell,
					                    "OpenInBrower Plug-in",
					                    "can not open "+osName+"ERROR OS.");
							}
						}else if("Gnome".equalsIgnoreCase(text)){
							if(osName.contains(LINUX)){
								openGnomeTerminal(path);
							}else if(osName.contains(WINDOWS)){
								openWinCmd(path);
							}else{
								MessageDialog.openInformation(
					                    shell,
					                    "OpenInBrower Plug-in",
					                    "can not open "+osName+"ERROR OS.");
							}
						}else if("Other".equalsIgnoreCase(text)){
							
						}
					} catch (Exception e) {
						MessageDialog.openInformation(
			                    shell,
			                    "OpenInBrower Plug-in",
			                    "can not open "+path+"ERROR MESSAGE"+e.getMessage());
						e.printStackTrace();
					}
				}else{
					MessageDialog.openInformation(
		                    shell,
		                    "OpenInBrower Plug-in",
		                    "can not open "+path);
				}
			}
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}
	
	private String getSelectionPath(Object o){
		String fpath = "";
//		IProject project = null;
//        if(o instanceof IAdaptable){
//        	fpath = ((IFolder)((IAdaptable)o).getAdapter(IFolder.class)).getLocation().toOSString();
//        }
        if(o instanceof IFolder){
        	fpath = ((IFolder)((IAdaptable)o).getAdapter(IFolder.class)).getLocation().toOSString();
        }
        if(o instanceof IFile){
        	fpath = ((IFile)o).getParent().getLocation().toOSString();
            }
        if(o instanceof File){
        	fpath = ((File)o).getParentFile().getAbsolutePath();
            }
        if(o instanceof IWorkbench){
        	MessageDialog.openInformation(
                    shell,
                    "OpenInBrower Plug-in",
                    "can not open "+fpath);
        }
		if (o instanceof IProject) {
			fpath = ((IProject) o).getLocation().toOSString();
		} 
		return fpath;
	}
	
	private void openExplorerWinXp(String path) throws Exception{
		openPath(EXPLORER_WINXP, path);
	}
	
	//TODO very slow..
	private void openWinCmd(String path) throws Exception {
		String cmd = CMD;
		if(path.contains("D:"))
			cmd += "D: & cd ";
		if(path.contains("E:"))
			cmd += "E: & cd ";
		openPath(cmd, path+"\"");
		
	}
	
	//open in explorer os is ubuntu
	private void openExplorerLinux(String path) throws Exception{
		openPath(EXPLORER_LINUX_UBUNTU, path);
    }
	
	//open in gnome-terminal os is ubunut
	private void openGnomeTerminal(String path) throws Exception{
		openPath(GNOME,path);
	}
	
	private void openPath(String order, String path) throws Exception{
		StringBuffer ep = new StringBuffer(order);
		ep.append(path);
		exec(ep.toString());
	}
	
	private void exec(String bat) throws IOException{
		Runtime.getRuntime().exec(bat);
	}

	

}
