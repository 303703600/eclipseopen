/*******************************************************************************
 * Copyright (c) 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package openin.sampleopen;


import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.part.ViewPart;


public class OpenInPage extends ViewPart implements IViewPart  {

	
	/**
	 * this for view style.
	 * 
	 */
	@Override
	public void createPartControl(Composite parent) {
		// TODO Auto-generated method stub
		TreeViewer tree = new TreeViewer(parent, SWT.BORDER);
        tree.setLabelProvider(new LabelProvider() {

            public Image getImage(Object element) {
                return null;
            }

            public String getText(Object element) {
                if (element instanceof IResource) {
                    return ((IResource) element).getName();
                }
                return "<no selection>";
            }
        });
        tree.setContentProvider(new ITreeContentProvider() {
        	
        	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
                
            }

            public Object[] getChildren(Object parentElement) {
                try {
                    if (parentElement instanceof IContainer) {
                        return ((IContainer) parentElement).members();
                    }
                } catch (CoreException e) {
                    e.printStackTrace();
                }
                return new Object[0];
            }

            public Object getParent(Object element) {
                return null;
            }

            public boolean hasChildren(Object element) {
                if (element instanceof IContainer) {
                    try {
                        return (((IContainer) element).members().length > 0);
                    } catch (CoreException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
                return false;
            }

            public Object[] getElements(Object inputElement) {
                IResource[] r = (IResource[]) inputElement;
                return r;
            }

            public void dispose() {
                
            }

            
        });
        Object[] os = ResourcesPlugin.getWorkspace().getRoot().getProjects();
        tree.setInput(os);
        
        //init popup menu
        MenuManager popupMenuManager = new MenuManager("#PopupMenu");  
        popupMenuManager.setRemoveAllWhenShown(true);  
        Menu popupMenu = popupMenuManager.createContextMenu(tree.getTree());  
        tree.getTree().setMenu(popupMenu);  
        //select tree and popup
        getSite().setSelectionProvider(tree);  
        getSite().registerContextMenu(popupMenuManager, tree);  
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}
	
	
}
